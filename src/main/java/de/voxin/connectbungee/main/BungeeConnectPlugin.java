package de.voxin.connectbungee.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.google.common.io.ByteStreams;

import de.voxin.connectbungee.listener.JoinListener;
import de.voxin.connectbungee.mysql.MySQLGateway;
import de.voxin.connectbungee.util.DataManager;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class BungeeConnectPlugin extends Plugin{
	
	private static @Getter BungeeConnectPlugin instance;
	
	private @Getter MySQLGateway mySQLGateway;
	private @Getter DataManager dataManager;
	
	private @Getter Configuration configuration;
	
	@Override
	public void onEnable() {
		instance = this;
		
		initConfig();
		initMySQL();
		
		dataManager = new DataManager(this);
		
		new JoinListener(this);
	
	}
	
	private void initConfig() {
		if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                try (InputStream is = getResourceAsStream("config.yml");
                     OutputStream os = new FileOutputStream(configFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file", e);
            }
        }
        
        try {
			configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initMySQL() {
		String host = configuration.getString("mysql.host");
		String database = configuration.getString("mysql.database");
		String user = configuration.getString("mysql.user");
		String password = configuration.getString("mysql.password");
		mySQLGateway = new MySQLGateway(host, database, user, password);
	}

}
