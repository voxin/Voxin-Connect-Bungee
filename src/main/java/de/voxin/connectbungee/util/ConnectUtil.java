package de.voxin.connectbungee.util;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import de.voxin.connectbungee.main.BungeeConnectPlugin;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ConnectUtil {

	public static boolean sendPluginMessage(ServerInfo server, String subchannel, List<String> players) {

		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF(subchannel);

		ByteArrayOutputStream messageBytes = new ByteArrayOutputStream();
		DataOutputStream messageOut = new DataOutputStream(messageBytes);
		String list = new String();
		for(String player : players){
			list += player + ", ";
		}
		
		try {
			messageOut.writeUTF(list);
		} catch (IOException e) {
			
		}

		out.writeShort(messageBytes.toByteArray().length);
		out.write(messageBytes.toByteArray());

		server.sendData("BungeeCord", out.toByteArray());

		return true;
	}

	public static void sendSyncPlayerList(ProxiedPlayer disconnected) {
		List<String> playerList = Lists.newArrayList();
		BungeeConnectPlugin.getInstance().getProxy().getPlayers().forEach(player -> {
			if(!player.equals(disconnected)){
				playerList.add(player.getName());
			}
		});
		BungeeConnectPlugin.getInstance().getProxy().getServers().values().forEach(server -> sendPluginMessage(server, "Voxin_SyncOnlinePlayers", playerList));
	}

}
