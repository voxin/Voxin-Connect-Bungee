package de.voxin.connectbungee.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import com.google.common.collect.Maps;

import de.voxin.connectbungee.main.BungeeConnectPlugin;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class DataManager {

	private BungeeConnectPlugin plugin;

	private Map<UUID, Date> joinData = Maps.newHashMap();

	public DataManager(BungeeConnectPlugin plugin) {
		this.plugin = plugin;
	}

	public void startSession(ProxiedPlayer player) {
		Date date = new Date();
		String ip = player.getAddress().getAddress().getHostAddress();

		joinData.put(player.getUniqueId(), date);

		PreparedStatement st = null;
		try {
			String updatePlayer = "INSERT INTO voxin_player (UUID, name, lastIP, lastSeen, created) "
					+ "VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE lastIP = ?, lastSeen = ?, name = ?;";

			st = plugin.getMySQLGateway().getConnection().prepareStatement(updatePlayer);
			st.setString(1, player.getUniqueId().toString());
			st.setString(2, player.getName());
			st.setString(3, ip);
			st.setTimestamp(4, new Timestamp(date.getTime()));
			st.setTimestamp(5, new Timestamp(date.getTime()));
			st.setString(6, ip);
			st.setTimestamp(7, new Timestamp(date.getTime()));
			st.setString(8, player.getName());
			st.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null && !st.isClosed()) {
					st.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	public void endSession(ProxiedPlayer player) {
		Date date = new Date();
		Date startDate = joinData.get(player.getUniqueId());

		PreparedStatement stSession = null;
		PreparedStatement stPlayerUpdate = null;
		try {
			String updateSession = "INSERT INTO voxin_session (UUID, joined, quit) VALUES (?, ?, ?);";
			String updatePlayer = "UPDATE voxin_player SET lastSeen = ?, playtime = playtime + ? WHERE UUID = ?;";
			stSession = plugin.getMySQLGateway().getConnection().prepareStatement(updateSession);
			stPlayerUpdate = plugin.getMySQLGateway().getConnection().prepareStatement(updatePlayer);
			stSession.setString(1, player.getUniqueId().toString());
			stSession.setTimestamp(2, new Timestamp(startDate.getTime()));
			stSession.setTimestamp(3, new Timestamp(date.getTime()));
			stSession.executeUpdate();
			stPlayerUpdate.setTimestamp(1, new Timestamp(date.getTime()));
			stPlayerUpdate.setLong(2, Math.toIntExact(date.getTime() - startDate.getTime()) / 1000);
			stPlayerUpdate.setString(3, player.getUniqueId().toString());
			stPlayerUpdate.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stSession != null && !stSession.isClosed()) {
					stSession.close();
				}
				if (stPlayerUpdate != null && !stPlayerUpdate.isClosed()) {
					stPlayerUpdate.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

}
