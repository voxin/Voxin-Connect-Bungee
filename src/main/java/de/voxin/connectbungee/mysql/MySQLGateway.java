package de.voxin.connectbungee.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLGateway {

	private Connection connection = null;

	private String host;
	private String database;
	private String user;
	private String password;
	
	public MySQLGateway(String host, String database, String user, String password) {
		connect(host, database, user, password);
		this.host = host;
		this.database = database;
		this.user = user;
		this.password = password;
	}
	
	public Connection getConnection(){
		try {
			if(!connection.isValid(10)){
				connect(host, database, user, password);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	private void connect(String host, String database, String user, String password) {
		try {
			this.connection = DriverManager.getConnection(
					"jdbc:mysql://" + host + "/" + database + "?" + "user=" + user + "&password=" + password);

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	public void closeConnection() {

		try {
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}

	}

}
