package de.voxin.connectbungee.listener;

import de.voxin.connectbungee.main.BungeeConnectPlugin;
import de.voxin.connectbungee.util.ConnectUtil;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class JoinListener implements Listener{

	private BungeeConnectPlugin plugin;
	
	public JoinListener(BungeeConnectPlugin plugin) {	
		this.plugin = plugin;
		plugin.getProxy().getPluginManager().registerListener(plugin, this);
	}
	
	@EventHandler
	public void onJoin(PostLoginEvent event){
		
		ConnectUtil.sendSyncPlayerList(null);
		plugin.getDataManager().startSession(event.getPlayer());
		
	}
	
	@EventHandler
	public void onQuit(PlayerDisconnectEvent event){
		
		ConnectUtil.sendSyncPlayerList(event.getPlayer());
		plugin.getDataManager().endSession(event.getPlayer());
		
	}

}
